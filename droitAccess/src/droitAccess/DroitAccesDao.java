package droitAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DroitAccesDao {
	
	private static final String DESACTIVE_ANCIENS_UTILISATEURS = "UPDATE Utilisateur SET actif = false WHERE YEAR(CURRENT_DATE) - YEAR(dateInscription) > 10";
	private static final String SELECT_UTILISATEURS_LIST = "SELECT * FROM Utilisateur";
	private Connection connection;
	
	public DroitAccesDao(Connection connection) {
		this.connection = connection;
	}
	
	public void desactiverAnciensUtilisateurs() throws SQLException{
			try (java.sql.Statement stmt = connection.createStatement()){
			stmt.executeUpdate(DESACTIVE_ANCIENS_UTILISATEURS);
			}
	}
	

	public List<Utilisateur> getUtilisateurs() throws SQLException{
		ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
		try (
			java.sql.Statement stmt = connection.createStatement();
			java.sql.ResultSet resultSet = stmt.executeQuery(SELECT_UTILISATEURS_LIST);) {		
			  while (resultSet.next()) {
				Utilisateur userDao = new Utilisateur();
				userDao.setId(resultSet.getInt("id"));
				userDao.setLogin(resultSet.getString("login")); 
			    userDao.setInscription(resultSet.getDate("dateInscription"));
			    userDao.setActif(resultSet.getBoolean("actif"));
			    listeUtilisateurs.add(userDao);
			  }
		}		
		return listeUtilisateurs;
	}
	
	public boolean isAutorise(String login, String droit) throws SQLException{
		  String request = "SELECT * FROM Utilisateur INNER JOIN Utilisateur_Droit ON id_utilisateur = Utilisateur.id INNER JOIN Droit ON Droit.id=Utilisateur_Droit.id_droit	 WHERE Droit.libelle = ? AND Utilisateur.login = ?";
		  try (java.sql.PreparedStatement stmt = connection.prepareStatement(request)) {

		    stmt.setString(1, droit);
		    stmt.setString(2, login);

		    try (java.sql.ResultSet resultSet = stmt.executeQuery()) {
		      return resultSet.next();
		    }
		  }
	}
	
	public boolean existeDroit(String droit) throws SQLException {
		  String request = "SELECT * FROM Droit WHERE libelle = ?";
		  try (java.sql.PreparedStatement pstmt = connection.prepareStatement(request)) {

		    pstmt.setString(1, droit);

		    try (java.sql.ResultSet resultSet = pstmt.executeQuery()) {
		      return resultSet.next();
		    }
		  } 
		
	}
	
	
	public int idDroit(String droit) throws SQLException{
		  String request = "SELECT id FROM Droit WHERE libelle = ?";
		  int idDroit = 0;
		  try (java.sql.PreparedStatement pstmt = connection.prepareStatement(request)) {

			    pstmt.setString(1, droit);

			    try (java.sql.ResultSet resultSet = pstmt.executeQuery()) {
			    	if (resultSet.next()) {
			    		idDroit = resultSet.getInt("id");
			    	}
			    }
			  }
		  return idDroit;
	}
	
	public void addUtilisateur(Utilisateur u, String... droits) throws SQLException, DroitInconnuException{
		connection.setAutoCommit(false);
		boolean transactionOk = false;
		try {
		  int key = 0;
		  String ajoutUser = "insert into Utilisateur (login, dateInscription, actif) values (?, ?, ?)";
		  try (java.sql.PreparedStatement pstmt = connection.prepareStatement(ajoutUser, java.sql.Statement.RETURN_GENERATED_KEYS)) {
		
			  	pstmt.setString(1, u.getLogin());
				pstmt.setDate(2, u.getInscription());
				pstmt.setBoolean(3, u.isActif());
				
				pstmt.executeUpdate();
				  
				try (java.sql.ResultSet resultSet = pstmt.getGeneratedKeys()) {
					  if (resultSet.next()) {
					      key = resultSet.getInt(1);
					  }
				}
		  }
		  
		  for(String droit: droits) {
			  
			  if(existeDroit(droit)) { 
				  String ajoutDroit = "insert into Utilisateur_Droit (id_utilisateur, id_droit) values (?, ?)";
				  try (java.sql.PreparedStatement pstmt = connection.prepareStatement(ajoutDroit)) {
						
					  	pstmt.setInt(1, key);
						pstmt.setInt(2, idDroit(droit));

						
						pstmt.executeUpdate();
						  
				  }
			  }else {
				  throw new DroitInconnuException(droit);
			  }
		  }
		  
		  transactionOk = true;
		  
		}finally {
		  if (transactionOk) {
			    connection.commit();
			  }
			  else {
			    connection.rollback();
			  }
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
