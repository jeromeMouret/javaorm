package droitAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class appli {

	public static void main(String[] args) throws DroitInconnuException {
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:8889/droitAcces",
	                											 "root", "root")){
			DroitAccesDao dao = new DroitAccesDao(connection);
			dao.desactiverAnciensUtilisateurs();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:8889/droitAcces",
				 "root", "root")){
			DroitAccesDao dao = new DroitAccesDao(connection);
			
			for(Utilisateur user : dao.getUtilisateurs())
			{
			  System.out.println(user.getId() + " " + user.getLogin() + " " + user.getInscription() + " " + user.isActif());

			}
		}catch(SQLException e){
		e.printStackTrace();
		}
		
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:8889/droitAcces",
				 "root", "root")){
				DroitAccesDao dao = new DroitAccesDao(connection);
				System.out.println(dao.isAutorise("doe", "connexion"));
				System.out.println(dao.isAutorise("de", "connexion"));
			}catch(SQLException e){
			e.printStackTrace();
			}
		
		
		
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:8889/droitAcces",
				 "root", "root")){
				DroitAccesDao dao = new DroitAccesDao(connection);
				Utilisateur user = new Utilisateur();
				Utilisateur user2 = new Utilisateur();
				user.setLogin("Neo");
				user.setInscription(new java.sql.Date(System.currentTimeMillis()));
				user.setActif(true);
				user2.setLogin("Hendrix");
				user2.setInscription(new java.sql.Date(System.currentTimeMillis()));
				user2.setActif(true);
				dao.addUtilisateur(user, "lecture", "ecriture");
				dao.addUtilisateur(user, "lecture", "ecriture", "BigBoss");
			}catch(SQLException e){
			e.printStackTrace();
			}
	}

}
