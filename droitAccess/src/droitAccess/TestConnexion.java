package droitAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestConnexion {

	public static void main(String[] args) {
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:8889/droitAcces",
                "root", "root")){
		
		
		connection.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

}
