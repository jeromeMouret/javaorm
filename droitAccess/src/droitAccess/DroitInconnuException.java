package droitAccess;

@SuppressWarnings("serial")
public class DroitInconnuException extends Exception {
	public DroitInconnuException(String droit) {
		System.out.println("Droit non existant :" + droit);
	}

}
