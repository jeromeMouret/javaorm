package jpaDroitAcces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaApp {

	public static void main(String[] args) throws ParseException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("droitAcces");
		try {
			
			EntityManager em = emf.createEntityManager();
			try {
				/* select 
				Utilisateur u = em.find(Utilisateur.class, 2);
				System.out.println(u.getLogin());
				System.out.println(u.getDateInscription());
				System.out.println(u.isActif());*/
				
				/* insert
				Utilisateur user = new Utilisateur();
				user.setLogin("rooot");
				user.setDateInscription(new Date());
				user.setActif(true);
				em.getTransaction().begin();
				em.persist(user);
				em.getTransaction().commit();*/
				
				/* delete
				Utilisateur u = em.find(Utilisateur.class, 9);
				em.getTransaction().begin();
				em.remove(u);
				em.getTransaction().commit();*/
				
				/* update 
				Utilisateur u = em.find(Utilisateur.class, 10);
				em.getTransaction().begin();
				u.setLogin("toto");
				em.getTransaction().commit();
				*/
				
				/* insert + anulation des modifs avant commit  
				Utilisateur user = new Utilisateur();
				user.setLogin("rooot");
				user.setDateInscription(new Date());
				user.setActif(true);
				
				em.refresh(user);  annule les modifs
				
				em.getTransaction().begin();
				em.persist(user);
				em.getTransaction().commit();*/
				
				/* insert + subtilité persist ( recupere auto l'id )
				Utilisateur user = new Utilisateur();
				user.setLogin("rooot");
				user.setDateInscription(new Date());
				user.setActif(true);
				em.getTransaction().begin();
				em.persist(user);
				em.getTransaction().commit();
				System.out.println(user.getId());
				*/

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY");
				Date date = simpleDateFormat.parse("03/06/2018");
				
				UtilisateurDao userDao = new UtilisateurDao(em);
				
				System.out.println(userDao.get(1).getBadge().getNumero());
				
				for(Utilisateur user : userDao.get()) {
					System.out.println(user.getLogin());
				}
				
				em.getTransaction().begin();
				userDao.desactiverInscritsAvant(date);
				em.getTransaction().commit();
			
				for(Utilisateur user : userDao.getActifs()) {
					System.out.println(user.getLogin());
				}
				
				System.out.println(userDao.getByBadge("A").getLogin());
				
				for(Utilisateur u: userDao.getByDroit("connexion")) {
					System.out.println(u.getLogin());
				}
				
				System.out.println(userDao.isAutorise("A", "connexion"));
				
				Utilisateur u = new Utilisateur();
				u.setLogin("b");
				u.setDateInscription(new Date());
				u.setActif(true);
				
				Badge badge = new Badge();
				badge.setNumero("#1288");
				u.setBadge(badge);
				
				em.getTransaction().begin();;
				userDao.create(u);
				em.getTransaction().commit();
				
				
			}finally {
				em.close();
			}		
		}finally {
			emf.close();
		}

	}

}
