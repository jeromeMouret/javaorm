package jpaDroitAcces;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;


public class UtilisateurDao {
	
	private EntityManager em;
	
	public UtilisateurDao(EntityManager em) {
		this.em = em;
	}
	
	public Utilisateur get(int id) {
		Utilisateur u = this.em.find(Utilisateur.class, id);
		return u;
	}
	
	public List<Utilisateur> get() {
		List<Utilisateur> users = null;
		users = this.em.createQuery("select u from Utilisateur u ", Utilisateur.class)
				.getResultList();
		return users;
	}
	
	public List<Utilisateur> getActifs() {
		List<Utilisateur> users = null;
		users = this.em.createQuery("select i from Utilisateur i where i.actif = :actif", Utilisateur.class)
				.setParameter("actif", true)
				.getResultList();
		return users;
	}
	
	public void desactiverInscritsAvant(Date date){
		
		this.em.createQuery("update Utilisateur i set i.actif = false where i.dateInscription < :date")
		.setParameter("date", date)
		.executeUpdate();
		
	}
	
	public Utilisateur getByBadge(String numeroBadge) {
		Utilisateur user = null;
		user = this.em.createQuery("select u from Utilisateur u where u.badge.numero = :badge", Utilisateur.class)
				.setParameter("badge", numeroBadge)
				.getSingleResult();
		return user;
	}

	public List<Utilisateur> getByDroit(String droit) {
		List<Utilisateur> user = null;
		user = this.em.createQuery("select u from Utilisateur u join u.droits d where d.libelle = :droit", Utilisateur.class)
				.setParameter("droit", droit)
				.getResultList();
		return user;
	}
	
	public boolean isAutorise(String numeroBadge, String droit) {
		long c =  this.em.createQuery("select count(u) from Utilisateur u join u.droits d where u.badge.numero = :numeroBadge and d.libelle = :droit", Long.class)
		.setParameter("droit", droit)
		.setParameter("numeroBadge", numeroBadge)
		.getSingleResult();
		
		return c == 1;
		
	}
	
	public void create(Utilisateur u) {
		em.persist(u);
	}
}
