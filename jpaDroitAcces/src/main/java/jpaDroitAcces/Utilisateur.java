package jpaDroitAcces;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Column;

@Entity
public class Utilisateur {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Basic
	private String login;
	
	
	@ManyToMany
	@JoinTable(name = "Utilisateur_Droit",
	joinColumns = @JoinColumn(name = "id_utilisateur"),
	inverseJoinColumns = @JoinColumn(name = "id_droit"))
	private List<Droit> droits = new ArrayList<>();

	
	
	@Temporal(TemporalType.DATE)
	private Date dateInscription;
	
	@Basic
	private boolean actif;
	
	
	@OneToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name = "id_badge")
	private Badge badge;

	public Badge getBadge() {
		return badge;
	}

	public void setBadge(Badge badge) {
		this.badge = badge;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}
	

	
	

}
